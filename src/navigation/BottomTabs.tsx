import {StyleSheet, View} from 'react-native';
import React from 'react';
import HomeScreen from '../screens/HomeScreen';
import FastImage from 'react-native-fast-image';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {ic_booking, ic_home, ic_ib, ic_offer, ic_user} from '../assets';

const Tab = createBottomTabNavigator();
export const BottomTabs = () => {
  return (
    <Tab.Navigator
      initialRouteName="HomeScreen"
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          backgroundColor: '#EC441E',
          shadowColor: '#000000',
          shadowOffset: {
            width: 0,
            height: 0,
          },
          shadowRadius: 3,
          shadowOpacity: 0.2,
          elevation: 1,
        },
        tabBarActiveTintColor: '#FFFFFF',
        tabBarInactiveTintColor: '#FAD3CA',
      }}>
      <Tab.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({focused}) => (
            <FastImage
              source={ic_home}
              resizeMode="contain"
              style={styles.icon}
              tintColor={focused ? '#FFFFFF' : '#FAD3CA'}
            />
          ),
        }}
      />
      <Tab.Screen
        name="BookingScreen"
        component={HomeScreen}
        options={{
          tabBarLabel: 'Booking',
          tabBarIcon: ({focused}) => (
            <FastImage
              source={ic_booking}
              resizeMode="contain"
              style={styles.icon}
              tintColor={focused ? '#FFFFFF' : '#FAD3CA'}
            />
          ),
        }}
      />
      <Tab.Screen
        name="OfferScreen"
        component={HomeScreen}
        options={{
          tabBarLabel: 'Offer',
          tabBarIcon: ({focused}) => (
            <FastImage
              source={ic_offer}
              resizeMode="contain"
              style={[styles.icon, {width: 20, aspectRatio: 1}]}
              tintColor={focused ? '#FFFFFF' : '#FAD3CA'}
            />
          ),
        }}
      />
      <Tab.Screen
        name="InboxScreen"
        component={HomeScreen}
        options={{
          tabBarLabel: 'Inbox',
          tabBarIcon: ({focused}) => (
            <FastImage
              source={ic_ib}
              resizeMode="contain"
              style={styles.icon}
              tintColor={focused ? '#FFFFFF' : '#FAD3CA'}
            />
          ),
        }}
      />
      <Tab.Screen
        name="ProfileScreen"
        component={HomeScreen}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({focused}) => (
            <FastImage
              source={ic_user}
              resizeMode="contain"
              style={styles.icon}
              tintColor={focused ? '#FFFFFF' : '#FAD3CA'}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  icon: {
    width: 24,
    aspectRatio: 1,
  },
});
