import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import SlashScreen from '../screens/SlashScreen';
import HomeScreen from '../screens/HomeScreen';
import {BottomTabs} from './BottomTabs';
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';
import SearchScreen from '../screens/SearchScreen';
import FlightDetailScreen from '../screens/FlightDetailScreen';
import ChooseSeat from '../screens/ChooseSeat';
import PersonalInfor from '../screens/PersonalInfor';

const RootNavigation = () => {
  const Stack = createStackNavigator();
  return (
    <>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="SlashScreen"
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen name="SlashScreen" component={SlashScreen} />
          <Stack.Screen name="BottomTabs" component={BottomTabs} />
          <Stack.Screen name="HomeScreen" component={HomeScreen} />
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
          <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
          <Stack.Screen name="SearchScreen" component={SearchScreen} />
          <Stack.Screen
            name="FlightDetailScreen"
            component={FlightDetailScreen}
          />
          <Stack.Screen name="ChooseSeat" component={ChooseSeat} />
          <Stack.Screen name="PersonalInfor" component={PersonalInfor} />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default RootNavigation;

const styles = StyleSheet.create({});
