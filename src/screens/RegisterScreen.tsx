import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';

const RegisterScreen = ({navigation}: any) => {
  const goToLogin = () => {
    navigation.navigate('LoginScreen');
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={{backgroundColor: 'blue', padding: 10, borderRadius: 5}}
        onPress={goToLogin}>
        <Text>Go to Login</Text>
      </TouchableOpacity>
    </View>
  );
};
export default RegisterScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});
