import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';
import {ic_arrow, ic_bgPlane} from '../assets';

const ChooseSeat = ({navigation}: any) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <FastImage
            source={ic_arrow}
            resizeMode="contain"
            style={styles.icon}
          />
        </TouchableOpacity>
        <Text style={styles.txtHeader}>Choose Seat</Text>
      </View>
      <View style={styles.rowScale}>
        <View style={styles.row}>
          <View style={styles.circle}></View>
          <Text style={styles.txtStatus}>Selected</Text>
        </View>
        <View style={styles.row}>
          <View style={[styles.circle, {backgroundColor: '#7C7270'}]}></View>
          <Text style={styles.txtStatus}>Emergency exit</Text>
        </View>
        <View style={styles.row}>
          <View style={[styles.circle, {backgroundColor: '#D9D9D9'}]}></View>
          <Text style={styles.txtStatus}>Reserved</Text>
        </View>
      </View>
      <FastImage
        source={ic_bgPlane}
        resizeMode="contain"
        style={styles.bgPlane}>
        <View
          style={{width: '100%', height: 380, backgroundColor: 'red'}}></View>
        <TouchableOpacity
          style={styles.btnConfirm}
          onPress={() => navigation.navigate('PersonalInfor')}>
          <Text style={[styles.txtBtn, {color: 'white'}]}>Confirm</Text>
        </TouchableOpacity>
      </FastImage>
    </View>
  );
};

export default ChooseSeat;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    backgroundColor: '#F9FBFA',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  txtHeader: {
    fontSize: 20,
    width: '90%',
    textAlign: 'center',
    paddingRight: 80,
  },
  icon: {
    width: 30,
    aspectRatio: 1,
  },
  rowScale: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    gap: 5,
  },
  circle: {
    width: 10,
    height: 10,
    backgroundColor: '#EC441E',
    borderRadius: 100,
  },
  txtStatus: {
    fontSize: 14,
    color: '#555555',
  },
  bgPlane: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },
  btnConfirm: {
    backgroundColor: '#EC441E',
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
    marginHorizontal: 40,
    marginTop: 20,
  },
  txtBtn: {
    fontSize: 18,
    fontWeight: '500',
    color: '#EC441E',
  },
});
