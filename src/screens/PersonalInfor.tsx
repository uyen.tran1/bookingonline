import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {
  ic_Calendar,
  ic_arrow,
  ic_ava,
  ic_dob,
  ic_earth,
  ic_map,
  ic_name,
  ic_passport,
} from '../assets';
import FastImage from 'react-native-fast-image';

const PersonalInfor = ({navigation}: any) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <FastImage
            source={ic_arrow}
            resizeMode="contain"
            style={styles.icon}
          />
        </TouchableOpacity>
        <Text style={styles.txtHeader}>Personal Info</Text>
      </View>
      <View style={styles.InforContainer}>
        <FastImage source={ic_ava} resizeMode="contain" style={styles.icAva} />
        <Text style={styles.txtName}>Hello Traveler</Text>
        <View style={styles.formContainer}>
          <View
            style={[
              styles.tipContainer,
              {
                marginTop: 20,
                alignItems: 'center',
                gap: 10,
              },
            ]}>
            <Text style={styles.txtTitle}>Name</Text>
            <FastImage
              source={ic_name}
              resizeMode="contain"
              style={styles.iconTip}
            />
            <TextInput placeholder="Enter your name here" />
          </View>
          <View
            style={[
              styles.tipContainer,
              {
                marginTop: 20,
                alignItems: 'center',
                gap: 10,
              },
            ]}>
            <Text style={styles.txtTitle}>Address</Text>
            <FastImage
              source={ic_map}
              resizeMode="contain"
              style={styles.iconTip}
            />
            <TextInput placeholder="Enter your address" />
          </View>
          <View
            style={[
              styles.tipContainer,
              {
                marginTop: 20,
                alignItems: 'center',
                gap: 10,
              },
            ]}>
            <Text style={styles.txtTitle}>Passport</Text>
            <FastImage
              source={ic_passport}
              resizeMode="contain"
              style={styles.iconTip}
            />
            <TextInput placeholder="ED 25265 589" />
          </View>
          <View
            style={[
              styles.tipContainer,
              {
                marginTop: 20,
                alignItems: 'center',
                gap: 10,
              },
            ]}>
            <Text style={styles.txtTitle}>DOB</Text>
            <FastImage
              source={ic_dob}
              resizeMode="contain"
              style={styles.iconTip}
            />
            <TextInput placeholder="12/05/1990" />
          </View>
          <View
            style={[
              styles.tipContainer,
              {
                marginTop: 20,
                alignItems: 'center',
                gap: 10,
              },
            ]}>
            <Text style={styles.txtTitle}>Country</Text>
            <FastImage
              source={ic_earth}
              resizeMode="contain"
              style={styles.iconTip}
            />
            <TextInput placeholder="Country" />
          </View>
        </View>
      </View>
      <View>
        <TouchableOpacity
          style={styles.btnConfirm}
          onPress={() => navigation.navigate('PersonalInfor')}>
          <Text style={[styles.txtBtn, {color: 'white'}]}>Confirm</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('PersonalInfor')}>
          <Text
            style={[
              styles.txtBtn,
              {
                fontWeight: '400',
                textAlign: 'center',
                fontSize: 16,
                marginTop: 10,
              },
            ]}>
            Skip
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default PersonalInfor;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    backgroundColor: '#F9FBFA',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  txtHeader: {
    fontSize: 20,
    width: '90%',
    textAlign: 'center',
    paddingRight: 80,
  },
  icon: {
    width: 30,
    aspectRatio: 1,
  },
  InforContainer: {
    marginTop: 40,
  },
  icAva: {
    width: 64,
    aspectRatio: 1,
    alignSelf: 'center',
  },
  txtName: {
    fontSize: 16,
    fontWeight: '500',
    textAlign: 'center',
    marginTop: 10,
  },
  formContainer: {
    padding: 30,
    gap: 10,
  },
  tipContainer: {
    borderWidth: 1,
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 14,
    borderColor: '#E6E8E7',
    borderRadius: 8,
  },
  iconTip: {
    width: 20,
    aspectRatio: 1,
  },
  txtTitle: {
    position: 'absolute',
    top: -10,
    backgroundColor: 'white',
    paddingHorizontal: 4,
    left: 10,
    color: '#555555',
  },
  btnConfirm: {
    backgroundColor: '#EC441E',
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
    marginHorizontal: 40,
    marginTop: 20,
  },
  txtBtn: {
    fontSize: 18,
    fontWeight: '500',
    color: '#EC441E',
  },
});
