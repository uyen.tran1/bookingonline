import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import FastImage from 'react-native-fast-image';
import {ic_Calendar, ic_arrow, ic_brand, ic_clock, ic_plane} from '../assets';
import {useRoute} from '@react-navigation/native';
const data = [
  {
    id: 1,
    image: ic_brand,
    sku: 'IN 230',
    time: '01 hr 40min',
    start: '5.50',
    end: '7.30',
    from: 'DEL (Delhi) ',
    to: 'CCU (Kolkata)',
    level: 'Business Class',
    price: '$230',
  },
  {
    id: 1,
    image: ic_brand,
    sku: 'IN 230',
    time: '01 hr 40min',
    start: '5.50',
    end: '7.30',
    from: 'DEL (Delhi) ',
    to: 'CCU (Kolkata)',
    level: 'Business Class',
    price: '$230',
  },
  {
    id: 1,
    image: ic_brand,
    sku: 'IN 230',
    time: '01 hr 40min',
    start: '5.50',
    end: '7.30',
    from: 'DEL (Delhi) ',
    to: 'CCU (Kolkata)',
    level: 'Business Class',
    price: '$230',
  },
];
const FlightDetailScreen = ({navigation}: any) => {
  // const route = useRoute();
  // const data = route.params;

  console.log(data && data[0]?.from);
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <FastImage
            source={ic_arrow}
            resizeMode="contain"
            style={styles.icon}
          />
        </TouchableOpacity>
        <Text style={styles.txtHeader}>Flight Details</Text>
      </View>
      {data && (
        <View style={styles.cardContainer}>
          <FastImage
            source={ic_brand}
            resizeMode="contain"
            style={styles.iconBrand}
          />
          <View style={styles.line}></View>
          <View style={styles.rowScale}>
            <View>
              <Text style={{fontWeight: '600', fontSize: 24}}>
                {data[0]?.start}
              </Text>
              <Text>{data[0]?.from}</Text>
            </View>
            <FastImage
              source={ic_plane}
              resizeMode="contain"
              style={styles?.iconPlane}
            />
            <View>
              <Text
                style={{textAlign: 'right', fontWeight: '600', fontSize: 24}}>
                {data[0]?.end}
              </Text>
              <Text>{data[0]?.to}</Text>
            </View>
          </View>
          <View style={[styles.rowScale, {marginTop: 20}]}>
            <View>
              <Text style={styles.txtAirport}>Indira Gandhi </Text>
              <Text style={styles.txtAirport}>International </Text>
              <Text style={styles.txtAirport}>Airport</Text>
            </View>
            <View>
              <Text style={[{textAlign: 'right'}, styles.txtAirport]}>
                Subhash Chandra
              </Text>
              <Text style={[{textAlign: 'right'}, styles.txtAirport]}>
                Bose International{' '}
              </Text>
              <Text style={[{textAlign: 'right'}, styles.txtAirport]}>
                Airport
              </Text>
            </View>
          </View>
          <View style={styles.line}></View>
          <View style={styles.rowScale}>
            <View
              style={[
                styles.tipContainer,
                {
                  marginTop: 20,
                  width: '48%',
                  alignItems: 'center',
                  gap: 10,
                },
              ]}>
              <Text style={styles.txtTitle}>Date</Text>
              <FastImage
                source={ic_Calendar}
                resizeMode="contain"
                style={styles.iconTip}
              />
              <Text>15/07/2024</Text>
            </View>
            <View
              style={[
                styles.tipContainer,
                {
                  marginTop: 20,
                  width: '48%',
                  alignItems: 'center',
                  gap: 10,
                },
              ]}>
              <Text style={styles.txtTitle}>Time</Text>
              <FastImage
                source={ic_clock}
                resizeMode="contain"
                style={styles.iconTip}
              />
              <Text>9.30</Text>
            </View>
          </View>
          <View style={styles.line}></View>
          <View style={styles.row}>
            <Text style={styles.txtPrice}>Price</Text>
            <Text style={styles.price}>$230</Text>
          </View>
        </View>
      )}
      <View style={styles.row}>
        <TouchableOpacity style={styles.btnCancel}>
          <Text style={styles.txtBtn}>Cancel</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btnConfirm}
          onPress={() => navigation.navigate('ChooseSeat')}>
          <Text style={[styles.txtBtn, {color: 'white'}]}>Confirm</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default FlightDetailScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    backgroundColor: '#F9FBFA',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  txtHeader: {
    fontSize: 20,
    width: '90%',
    textAlign: 'center',
    paddingRight: 80,
  },
  icon: {
    width: 30,
    aspectRatio: 1,
  },
  cardContainer: {
    backgroundColor: 'white',
    borderRadius: 16,
    marginHorizontal: 20,
    paddingVertical: 20,
    marginTop: 40,
  },
  iconBrand: {
    width: 100,
    aspectRatio: 1,
    alignSelf: 'center',
  },
  rowScale: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    marginVertical: 20,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    gap: 10,
  },
  iconPlane: {
    width: 120,
    height: 40,
  },
  txtAirport: {
    fontSize: 12,
    color: '#666666',
  },
  line: {
    borderTopWidth: 1,
    borderTopColor: '#E6E8E7',
  },
  tipContainer: {
    borderWidth: 1,
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderColor: '#E6E8E7',
    borderRadius: 8,
  },
  iconTip: {
    width: 20,
    aspectRatio: 1,
  },
  txtTitle: {
    position: 'absolute',
    top: -10,
    backgroundColor: 'white',
    paddingHorizontal: 4,
    left: 10,
    color: '#555555',
  },
  txtPrice: {
    fontSize: 22,
  },
  price: {
    fontSize: 32,
    fontWeight: '600',
  },
  btnCancel: {
    width: '48%',
    borderWidth: 1,
    borderColor: '#EC441E',
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
  },
  btnConfirm: {
    width: '48%',
    backgroundColor: '#EC441E',
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
  },
  txtBtn: {
    fontSize: 18,
    fontWeight: '500',
    color: '#EC441E',
  },
});
