import {FlatList, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import FastImage from 'react-native-fast-image';
import {ic_Calendar, ic_ham, ic_image, ic_landing, ic_takeoff} from '../assets';
import {TouchableOpacity} from 'react-native-gesture-handler';

const data = [
  {
    id: 1,
    title: '15% discount with mastercard',
    content: 'Lorem ipsum dolor sit am etet adip',
    image: ic_image,
  },
  {
    id: 2,
    title: '15% discount with mastercard',
    content: 'Lorem ipsum dolor sit am etet adip',
    image: ic_image,
  },
];
const HomeScreen = ({navigation}: any) => {
  const goToSearchScreen = () => {
    navigation.navigate('SearchScreen' as never);
  };
  const renderItem = ({item, index}: any) => {
    return (
      <View style={styles.cardContainer} key={item.id}>
        <FastImage source={item.image} style={styles.image} />
        <View style={{width: '50%', gap: 10}}>
          <Text style={styles.txtName}>15% discount with mastercard</Text>
          <Text style={{fontSize: 12, color: '#828282'}}>
            Lorem ipsum dolor sit am etet adip
          </Text>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.txtHeader}>Book Flight</Text>
        <FastImage source={ic_ham} resizeMode="contain" style={styles.icon} />
      </View>
      <View style={styles.tabContainer}>
        <View style={styles.active}>
          <Text style={{color: 'white'}}>One way</Text>
        </View>
        <Text style={{color: '#999999'}}>Round</Text>
        <Text style={{color: '#999999'}}>Multicity</Text>
      </View>
      <View style={styles.oneWayContainer}>
        <View style={styles.tipContainer}>
          <Text style={styles.txtTitle}>From</Text>
          <FastImage
            source={ic_takeoff}
            resizeMode="contain"
            style={styles.iconTip}
          />
          <View style={styles.txtContainer}>
            <Text style={styles.txtName}>TP.HCM</Text>
            <Text>Tan Son Nhat Airport</Text>
          </View>
        </View>
        <View style={[styles.tipContainer, {marginTop: 20}]}>
          <Text style={styles.txtTitle}>To</Text>
          <FastImage
            source={ic_landing}
            resizeMode="contain"
            style={styles.iconTip}
          />
          <View style={styles.txtContainer}>
            <Text style={styles.txtName}>Ha Noi</Text>
            <Text>Noi Bai Airport</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View
            style={[
              styles.tipContainer,
              {
                marginTop: 20,
                width: '48%',
                alignItems: 'center',
                gap: 10,
              },
            ]}>
            <Text style={styles.txtTitle}>Departure</Text>
            <FastImage
              source={ic_Calendar}
              resizeMode="contain"
              style={styles.iconTip}
            />
            <Text>15/07/2024</Text>
          </View>
          <View
            style={[
              styles.tipContainer,
              {
                marginTop: 20,
                width: '48%',
                alignItems: 'center',
                gap: 10,
              },
            ]}>
            <Text style={styles.txtTitle}>Return</Text>
            <Text>18/07/2024</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View
            style={[
              styles.tipContainer,
              {
                marginTop: 20,
                width: '48%',
                alignItems: 'center',
                gap: 10,
              },
            ]}>
            <Text style={styles.txtTitle}>Travel</Text>
            <Text style={styles.txtName}>1 Adult</Text>
          </View>
          <View
            style={[
              styles.tipContainer,
              {
                marginTop: 20,
                width: '48%',
                alignItems: 'center',
                gap: 10,
              },
            ]}>
            <Text style={styles.txtTitle}>Class</Text>
            <Text style={styles.txtName}>Economy</Text>
          </View>
        </View>
        <TouchableOpacity style={styles.btnSearch} onPress={goToSearchScreen}>
          <Text style={{color: 'white', fontSize: 18, fontWeight: '600'}}>
            Search
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.textContain}>
        <Text style={styles.txtOffer}>Hot offer</Text>
        <TouchableOpacity>
          <Text style={styles.txtSeeAll}>See all</Text>
        </TouchableOpacity>
      </View>
      <FlatList
        data={data}
        renderItem={renderItem}
        horizontal
        contentContainerStyle={styles.flatList}
        style={{flexGrow: 1, width: '100%'}}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    paddingHorizontal: 20,
    backgroundColor: '#F9FBFA',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
  },
  txtHeader: {
    fontSize: 20,
    width: '90%',
    textAlign: 'center',
    paddingLeft: 40,
  },
  icon: {
    width: 30,
    aspectRatio: 1,
  },
  tabContainer: {
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'space-between',
    marginTop: 40,
    borderRadius: 32,
    alignItems: 'center',
    paddingRight: 20,
  },
  active: {
    backgroundColor: '#EC441E',
    height: '100%',
    paddingHorizontal: 10,
    paddingVertical: 6,
    borderRadius: 32,
  },
  oneWayContainer: {
    backgroundColor: 'white',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.17,
    shadowRadius: 3.05,
    elevation: 4,
    padding: 20,
    borderRadius: 16,
    marginTop: 20,
  },
  tipContainer: {
    borderWidth: 1,
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderColor: '#E6E8E7',
    borderRadius: 8,
  },
  iconTip: {
    width: 20,
    aspectRatio: 1,
  },
  txtContainer: {
    paddingLeft: 10,
  },
  txtTitle: {
    position: 'absolute',
    top: -10,
    backgroundColor: 'white',
    paddingHorizontal: 4,
    left: 10,
    color: '#555555',
  },
  txtName: {
    fontWeight: '600',
  },
  btnSearch: {
    backgroundColor: '#EC441E',
    marginTop: 20,
    paddingVertical: 10,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
  },
  txtOffer: {
    fontWeight: '600',
    fontSize: 20,
  },
  textContain: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
    paddingVertical: 20,
    borderTopWidth: 1,
    borderTopColor: '#E6E8E7',
  },
  txtSeeAll: {
    color: '#EC441E',
    fontWeight: '500',
  },
  flatList: {
    gap: 10,
    width: '80%',
  },
  cardContainer: {
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.17,
    shadowRadius: 3.05,
    elevation: 4,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: 8,
    height: 100,
    alignItems: 'center',
    gap: 10,
    paddingRight: 20,
  },
  image: {
    width: 100,
    height: 100,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },
});
