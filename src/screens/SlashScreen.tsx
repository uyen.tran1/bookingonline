import {StyleSheet, View} from 'react-native';
import React, {useEffect} from 'react';
import {Logo} from '../assets';
import FastImage from 'react-native-fast-image';

const SlashScreen = ({navigation}: any) => {
  useEffect(() => {
    const timeoutId = setTimeout(() => {
      navigation.navigate('LoginScreen' as never);
    }, 3000);

    return () => clearTimeout(timeoutId);
  }, [navigation]);

  return (
    <View style={styles.container}>
      <FastImage source={Logo} resizeMode="contain" style={styles.icon} />
    </View>
  );
};

export default SlashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#EC441E',
  },
  icon: {
    width: 200,
    aspectRatio: 1,
  },
});
