import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import FastImage from 'react-native-fast-image';
import {ic_arrow, ic_brand, ic_chair, ic_plane} from '../assets';
import {FlatList} from 'react-native-gesture-handler';

const data = [
  {
    id: 1,
    image: ic_brand,
    sku: 'IN 230',
    time: '01 hr 40min',
    start: '5.50',
    end: '7.30',
    from: 'DEL (Delhi) ',
    to: 'CCU (Kolkata)',
    level: 'Business Class',
    price: '$230',
  },
  {
    id: 1,
    image: ic_brand,
    sku: 'IN 230',
    time: '01 hr 40min',
    start: '5.50',
    end: '7.30',
    from: 'DEL (Delhi) ',
    to: 'CCU (Kolkata)',
    level: 'Business Class',
    price: '$230',
  },
  {
    id: 1,
    image: ic_brand,
    sku: 'IN 230',
    time: '01 hr 40min',
    start: '5.50',
    end: '7.30',
    from: 'DEL (Delhi) ',
    to: 'CCU (Kolkata)',
    level: 'Business Class',
    price: '$230',
  },
];
const SearchScreen = ({navigation}: any) => {
  const handleCheck = () => {
    navigation.navigate('FlightDetailScreen', {data: data});
  };
  const renderItem = ({item}: any) => {
    return (
      <View style={styles.cardContainer} key={item.id}>
        <View style={styles.rowScale}>
          <View style={styles.row}>
            <FastImage
              source={item.image}
              resizeMode="contain"
              style={styles.iconBrand}
            />
            <Text>{item.sku}</Text>
          </View>
          <Text>{item.time}</Text>
        </View>
        <View style={styles.rowScale}>
          <View>
            <Text style={{fontWeight: '600'}}>{item.start}</Text>
            <Text>{item.from}</Text>
          </View>
          <FastImage
            source={ic_plane}
            resizeMode="contain"
            style={styles.iconPlane}
          />
          <View>
            <Text style={{textAlign: 'right', fontWeight: '600'}}>
              {item.end}
            </Text>
            <Text>{item.to}</Text>
          </View>
        </View>
        <View
          style={[
            styles.rowScale,
            {
              marginTop: 10,
              borderTopWidth: 1,
              borderTopColor: '#E6E8E7',
              paddingTop: 20,
            },
          ]}>
          <View style={styles.row}>
            <FastImage
              source={ic_chair}
              resizeMode="contain"
              style={styles.iconChair}
            />
            <Text>{item.level}</Text>
          </View>
          <View style={styles.row}>
            <Text>From</Text>
            <Text>{item.price}</Text>
          </View>
        </View>
        <TouchableOpacity style={styles.btnSearch} onPress={handleCheck}>
          <Text style={styles.txtSearch}>Check</Text>
        </TouchableOpacity>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <FastImage
            source={ic_arrow}
            resizeMode="contain"
            style={styles.icon}
          />
        </TouchableOpacity>
        <Text style={styles.txtHeader}>Search Result</Text>
      </View>
      <FlatList
        style={styles.flatList}
        data={data}
        renderItem={renderItem}
        contentContainerStyle={styles.contentStyle}
      />
    </View>
  );
};

export default SearchScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    backgroundColor: '#F9FBFA',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  txtHeader: {
    fontSize: 20,
    width: '90%',
    textAlign: 'center',
    paddingRight: 80,
  },
  icon: {
    width: 30,
    aspectRatio: 1,
  },
  cardContainer: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 16,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.17,
    shadowRadius: 3.05,
    elevation: 4,
    width: '98%',
    alignSelf: 'center',
    paddingHorizontal: 20,
  },
  rowScale: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    gap: 5,
  },
  iconBrand: {
    width: 40,
    aspectRatio: 1,
  },
  flatList: {
    flexGrow: 1,
    paddingHorizontal: 10,
    marginTop: 20,
  },
  iconPlane: {
    width: 140,
    height: 40,
  },
  iconChair: {
    width: 16,
    aspectRatio: 1,
  },
  btnSearch: {
    backgroundColor: '#EC441E',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 8,
    marginTop: 20,
  },
  txtSearch: {
    fontSize: 18,
    fontWeight: '600',
    color: 'white',
  },
  contentStyle: {
    gap: 20,
    marginBottom: 100,
  },
});
