import React, {useState} from 'react';

import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';

import CheckBox from '@react-native-community/checkbox';

const LoginScreen = ({navigation}: any) => {
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [hidePassword, setHidePassword] = useState(true);
  const [isChecked, setIsChecked] = useState(false);
  const [loginMethod, setLoginMethod] = useState('Email');

  const goToRegister = () => {
    navigation.navigate('RegisterScreen');
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.text}>Login</Text>
        <Text style={styles.subText}>Welcome back to the app</Text>
      </View>
      <View style={styles.form}>
        <View style={styles.loginMethod}>
          <TouchableOpacity onPress={() => setLoginMethod('Email')}>
            <Text style={styles.loginMethodText}>Email</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setLoginMethod('Phone')}>
            <Text style={styles.loginMethodText}>Phone Number</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.inputForm}>
          {loginMethod === 'Email' ? (
            <View>
              <Text style={styles.textLale}>Email</Text>
              <TextInput
                style={styles.input}
                onChangeText={text => setEmail(text)}
              />
            </View>
          ) : (
            <View>
              <Text style={styles.textLale}>Phone Number</Text>
              <View style={styles.phoneContainer}>
                <TouchableOpacity>
                  <Text style={styles.phoneCountryCode}>+91</Text>
                </TouchableOpacity>
                <TextInput
                  style={styles.inputPhone}
                  onChangeText={text => setPhone(text)}
                />
              </View>
            </View>
          )}

          <View style={styles.passwordContainer}>
            <Text style={styles.textLale}>Password</Text>
            <TouchableOpacity style={styles.textLale}>
              <Text style={styles.textLalePassword}>Forgot Password?</Text>
            </TouchableOpacity>
          </View>
          <TextInput
            style={styles.input}
            secureTextEntry={hidePassword}
            onChangeText={text => setPassword(text)}
          />
        </View>
        <View style={styles.checkRememberme}>
          <CheckBox
            style={styles.checkbox}
            value={isChecked}
            onValueChange={setIsChecked}
            tintColors={{true: '#F15927', false: 'black'}}
          />
          <Text style={styles.textLale}>Keep me signed in</Text>
        </View>

        <TouchableOpacity
          style={styles.loginButton}
          onPress={navigation.navigate('BottomTabs')}>
          <Text style={styles.lblLogin}>Login</Text>
        </TouchableOpacity>
        <View style={styles.tileLine}>
          <View style={styles.lineView} />
          <Text style={styles.lblLineView}>or sign in with </Text>
        </View>

        <TouchableOpacity style={styles.googleButton}>
          <Image
            source={require('../assets/google.png')}
            style={{width: 24, height: 24}}
          />
          <Text style={styles.lblGG}>Continue with Google</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={goToRegister}>
          <Text style={styles.creatAccount}>Create an account</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    backgroundColor: '#F5FCFF',
  },

  header: {
    marginTop: 80,
  },

  text: {
    fontSize: 28,
    lineHeight: 40,
    fontWeight: '700',
    fontFamily: 'Inter-SemiBold',
    color: '#191919',
  },

  subText: {
    fontSize: 18,
    lineHeight: 26,
    fontFamily: 'Inter-Regular',
    color: '#555',
    textAlign: 'left',
    width: 312,
    marginTop: 12,
  },

  form: {
    marginTop: 48,
  },

  loginMethod: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 200,
    lineHeight: 40,
  },

  loginMethodText: {
    fontSize: 18,
    lineHeight: 24,
    fontFamily: 'Inter-Regular',
    color: '#555',
  },

  inputForm: {
    marginTop: 24,
  },

  input: {
    backgroundColor: '#F5F5F5',
    borderRadius: 8,
    padding: 16,
    marginTop: 8,
    fontSize: 16,
    color: '#928fa6',
  },

  phoneContainer: {
    flexDirection: 'row',
    backgroundColor: '#F5F5F5',
    marginTop: 8,
    alignItems: 'center',
    paddingHorizontal: 10,
  },

  phoneCountryCode: {
    fontSize: 16,
    color: '#928fa6',
  },

  inputPhone: {
    backgroundColor: '#F5F5F5',
    borderRadius: 8,
    padding: 16,
    marginTop: 8,
    fontSize: 16,
    color: '#928fa6',
    width: 300,
  },

  textLale: {
    fontSize: 16,
    lineHeight: 24,
    fontWeight: '500',
    fontFamily: 'Inter-Medium',
    color: '#191d23',
    textAlign: 'left',
    overflow: 'hidden',
  },

  passwordContainer: {
    width: 352,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 8,
  },

  textLalePassword: {
    fontSize: 12,
    lineHeight: 24,
    fontWeight: '500',
    fontFamily: 'Inter-Medium',
    color: '#ec441e',
    textAlign: 'right',
    width: 162,
  },

  checkbox: {
    marginRight: 8,
  },

  checkRememberme: {
    marginTop: 24,
    flexDirection: 'row',
    fontSize: 16,
    lineHeight: 24,
    fontWeight: '300',
    fontFamily: 'Inter-Light',
    color: '#191d23',
    textAlign: 'left',

    alignItems: 'center',
  },

  loginButton: {
    backgroundColor: '#F15927',
    borderRadius: 8,
    padding: 16,
    marginTop: 16,
    alignItems: 'center',
  },

  lblLogin: {
    fontSize: 16,
    lineHeight: 24,
    fontFamily: 'Inter-Medium',
    color: '#fff',
  },

  lineView: {
    position: 'absolute',
    top: 12,
    borderStyle: 'solid',
    borderColor: '#4b5768',
    borderTopWidth: 1,
    flex: 1,
    width: '100%',
    height: 0.5,
    opacity: 0.5,
  },

  tileLine: {
    marginTop: 24,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  lblLineView: {
    fontSize: 16,
    lineHeight: 24,
    fontFamily: 'Inter-Regular',
    color: '#4b5768',
    textAlign: 'center',
    width: 150,
    backgroundColor: '#F5FCFF',
  },

  googleButton: {
    flexDirection: 'row',
    backgroundColor: '#e4e7eb',
    borderRadius: 8,
    padding: 16,
    marginTop: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },

  lblGG: {
    fontSize: 16,
    lineHeight: 24,
    fontFamily: 'Inter-Regular',
    color: '#4b5768',
    textAlign: 'center',
    marginLeft: 16,
  },

  creatAccount: {
    fontSize: 16,
    lineHeight: 24,
    fontWeight: '700',
    fontFamily: 'Inter-SemiBold',
    color: '#ec441e',
    textAlign: 'center',
    overflow: 'hidden',
    marginTop: 24,
    height: 24,
  },
});
